# Terraspace

- [Terraspace](#terraspace)
  - [Qu'est-ce que Terraspace ?](#quest-ce-que-terraspace-)
    - [Fonctionnalités](#fonctionnalités)
  - [Installer les prérequis](#installer-les-prérequis)
    - [Installer Terraspace](#installer-terraspace)
      - [Installer sur votre machine local](#installer-sur-votre-machine-local)
        - [Mac OSX](#mac-osx)
        - [CentOS/RedHat/Fedora](#centosredhatfedora)
        - [Ubuntu/Debian](#ubuntudebian)
      - [Docker](#docker)
      - [Installer avec RubyGems](#installer-avec-rubygems)
    - [Installer Terraform](#installer-terraform)
      - [Installer via tfenv](#installer-via-tfenv)
      - [Installer via le téléchargement](#installer-via-le-téléchargement)
    - [Vérifier l'installation de Terraspace](#vérifier-linstallation-de-terraspace)
    - [Configurer `AWS_PROFILE` et `AWS_REGION` comme environnement de variable](#configurer-aws_profile-et-aws_region-comme-environnement-de-variable)
    - [Tester la configuration AWS](#tester-la-configuration-aws)
  - [Créer votre premier projet](#créer-votre-premier-projet)
    - [Comprendre l'arborescence de notre premier projet](#comprendre-larborescence-de-notre-premier-projet)
  - [Déployer votre premier projet](#déployer-votre-premier-projet)
  - [Changement et mise à jour de notre infrastructure](#changement-et-mise-à-jour-de-notre-infrastructure)
  - [Détruire notre infrastructure](#détruire-notre-infrastructure)
  - [Conlusion](#conclusion)

## Qu'est-ce que Terraspace ?

`Terraspace` est un framework (open-source) pour Terraform qui permet d'optimiser la structure de votre code. Compatible avec de nombreux fournisseurs de Cloud (Azure, GCP, etc.), il facilite la création et la gestion d'infrastructures en tant que code (IaC).

### Fonctionnalités

`Terraspace` offre plusieurs fonctionnalités comme :

- `DRY` (Don't Repeat Yourself) : c'est lorsqu'on va éviter la duplication de code au maximum.
- `Generators` : fourni la possibilité de créer des modules rapidement pour se concentrer sur notre code.
- `Multiple environments` : lorsqu'on exécute du code Terraform, on produit un unique fichier **tfvars**. Terraspace fournira la possibilité de produire plusieurs fichiers **tfvars** pour différents environnements (Exemple: production, test, développement).
- `Deploy Multiple Stacks` : fourni la possibilité de déployer plusieurs **stack** avec une seule commande. Terraspace calculera de lui-même les dépendances associées et l'ordre de déploiement. On peut bien évidemment déployer une stack unique plutôt que l'ensemble des stacks de notre répertoire.
- `Secrets Support` : Terraspace met à disposition l'intégration de la gestion des mots de passe des différents cloud provider (Azure Key Vault, AWS Secrets Manager).
- `Terrafile` : possibilité d'utiliser Git pour gérer nos modules Teraform.
- `Configurable CLI` : des commandes spécifiques qui vont nous permettre de gérer le cycle de vie de notre code Terraform.
- `Testing` : des tests uniques qui vont vous permettre d'assurer de la bonne faisabilité de votre code Terraform.

## Installer les prérequis

Pour utiliser Terraspace, vous aurez besoin d'installer Terraspace et Terraform. A noter qu'il n'y a pas d'ordre d'installation.

### Installer Terraspace

#### Installer sur votre machine local

##### Mac OSX

```bash
brew tap boltops-tools/software
brew install terraspace
More info on Mac OSX Install
```

##### CentOS/RedHat/Fedora

Pour l'installation, vous aurez besoin d'un utilisateur disposant des privilèges **sudo**.

```bash
curl -so /etc/yum.repos.d/boltops.repo https://yum.boltops.com/boltops.repo
rpm --import https://yum.boltops.com/boltops-key.public
yum install -y terraspace
```

##### Ubuntu/Debian

Un utilisateur disposant des privilèges **sudo** sera nécessaire pour effectuer l'installation.

```bash
echo "deb https://apt.boltops.com stable main" > /etc/apt/sources.list.d/boltops.list
curl -s https://apt.boltops.com/boltops-key.public | apt-key add -
apt-get update
apt-get install -y terraspace
```

#### Docker

```bash
docker run -ti boltops/terraspace:alpine
docker run -ti boltops/terraspace:amzn2
docker run -ti boltops/terraspace:centos
docker run -ti boltops/terraspace:debian
docker run -ti boltops/terraspace:fedora
docker run -ti boltops/terraspace:ubuntu
```

Lorsque vous utiliser Terraspace avec une image Docker, vous aurez besoin d'utiliser des volumes pour que votre conteneur contiennent vos credentiels ainsi que l'arborescence des fichiers Terraspace :

```bash
docker run --rm -ti -v $HOME/.aws:/root/.aws -v $pwd/terraspace:/root/terraspace ghcr.io/boltops-tools/terraspace:ubuntu
```

#### Installer avec RubyGems

Vous devrez avoir installé Ruby3.0 au minimum sur votre machine.

```bash
gem install terraspace
```

### Installer Terraform

#### Installer via tfenv

Terraspace recommande d'installer Terraform avec `tfenv`, cela nous permet de choisir et de pouvoir changer de version de Terraform de façon très simple. Lien vers [tfenv](https://github.com/tfutils/tfenv).

```bash
git clone https://github.com/tfutils/tfenv.git ~/.tfenv
echo 'export PATH="$HOME/.tfenv/bin:$PATH"' >> ~/.bash_profile
export PATH="$HOME/.tfenv/bin:$PATH"
tfenv install latest
tfenv use latest
```

#### Installer via le téléchargement

Sur ce [lien](https://developer.hashicorp.com/terraform/downloads), vous aurez les différentes instructions pour télécharger et installer Terraform suivant l'OS utilisé.

### Vérifier l'installation de Terraspace

Une fois tout les prérequis installés, vérifier l'installation à l'aide de la commmande :

```bash
terraspace setup check
```

```bash
Detected Terraspace version: 2.2.5

Detected Terraform bin: /home/debian/.tfenv/bin/terraform

Detected Terraform v1.4.0

Terraspace requires Terraform v0.12.x and above

You're all set!

```

## Configurer AWS

Dans cet exemple, nous utiliserons une machine Linux ainsi qu'AWS. Ici, on configurera AWS pour que Terraspace puisse s'y connecter.

### Configurer `~/.aws/config` et `~/.credentials`

Dans cette partie, vous aurez besoin d'installer [aws cli](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html).

Ici, on va configurer un profil de développement. Lien vers la [documentation](https://docs.aws.amazon.com/cli/latest/userguide/cli-configure-files.html). On utilisera la région Paris (eu-west-3). Le contenu du fichier `~/.aws/config` :

```bash
[profile dev]
output = json
region = eu-west-3
```

Le contenu du fichier `~/.credentials`, il vous faudra générer un [utilisateur](https://docs.aws.amazon.com/fr_fr/IAM/latest/UserGuide/id_users_create.html) sur IAM dans la console AWS au préalable et créer une [clef d'accès](https://docs.aws.amazon.com/fr_fr/IAM/latest/UserGuide/id_credentials_access-keys.html).

Dans cette exemple, l'utilisateur IAM aura besoin des droits suivants :

- AmazonS3FullAccess
- AmazonDynamoDBFullAccess

```bash
[dev]
aws_access_key_id = REPLACE_ME
aws_secret_access_key = REPLACE_ME
```

### Configurer `AWS_PROFILE` et `AWS_REGION` comme environnement de variable

Ensuite, vous aurez besoin de configurer `AWS_PROFILE` et `AWS_REGION` en tant que variable d'environnement pour pouvoir utiliser Terraform par la suite :

```bash
export AWS_PROFILE=dev
export AWS_REGION=`aws configure get region`
```

Vous pouvez vérifier que vos variables d'environnement ont bien été configurées :

```bash
env | grep "AWS"
```

### Tester la configuration AWS

Vérifier la configuration de votre profil AWS :

```bash
aws sts get-caller-identity
```

## Créer votre premier projet

Après avoir configurer notre profil AWS, il est maintenant temps de créer notre premier projet.

Initialiser un workspace de travail :

```bash
mkdir -p ~/workspace/tuto/terraspace
cd ~/workspace/tuto/terraspace
```

Utilisez la commande suivante depuis votre terminal :

```bash
terraspace new project NAME
```

Terraspace fourni un plugin aws pour créer un premier projet qui peut servir d'exemple :

```bash
terraspace new project NAME --plugin aws --examples
```

La liste des commandes utilisables pour créer un projet :

```bash
    [--examples], [--no-examples]  # Also generate examples
y, [--force]                       # Bypass overwrite are you sure prompt for existing files.
    [--lang=LANG]                  # Language to use: HCL/ERB or Ruby DSL
                                   # Default: hcl
p, [--plugin=PLUGIN]               # Cloud Plugin. Supports: aws, google
                                   # Default: aws
    [--test], [--no-test]          # Whether or not to generate tests
    [--plugin-gem=PLUGIN_GEM]      # Use if provider gem name doesnt follow terraspace_plugin_XXX naming convention. Must specify both --plugin and --plugin-name option
    [--bundle], [--no-bundle]      # Runs bundle install on the project
                                   # Default: true
    [--config], [--no-config]      # Whether or not to generate config files.
                                   # Default: true
    [--quiet], [--no-quiet]        # Quiet output.
```

### Comprendre l'arborescence de notre premier projet

Après avoir créer notre projet, on obtiendra l'arborescence suivante :

```tree
.
└── NAME
    ├── app
    │   ├── modules
    │   │   └── bucket_s3
    │   │       ├── main.tf
    │   │       ├── outputs.tf
    │   │       └── variables.tf
    │   └── stacks
    │       └── demo
    │           ├── main.tf
    │           ├── outputs.tf
    │           └── variables.tf
    ├── config
    │   ├── app.rb
    │   └── terraform
    │       ├── backend.tf
    │       └── provider.tf
    ├── Gemfile
    ├── Gemfile.lock
    ├── README.md
    └── Terrafile
```

Cet exemple est fourni par Terraspace. Les fichiers Terraform qui nous permettront de créer un bucket S3 se trouvent dans le dossier **modules/bucket_s3**.

Le fichier `app/modules/bucket_s3/main.tf`

```bash
resource "aws_s3_bucket" "this" {
  bucket = var.bucket # If omitted, Terraform will assign a random, unique name.
}

resource "aws_s3_bucket_acl" "this" {
  bucket = aws_s3_bucket.this.id
  acl    = var.acl
}
```

Le fichier `app/modules/bucket_s3/variables.tf`

```bash
variable "bucket" {
  description = "The name of the bucket. If omitted, Terraform will assign a random, unique name." # IE: terraform-2020052606510241590000000
  type        = string
  default     = null
}

variable "acl" {
  description = "The canned ACL to apply. Defaults to 'private'."
  type        = string
  default     = "private"
}
```

Le dossier **stacks/demo** contient des fichiers Terraform qui utilise le dossier **modules** pour nous permettre de créer notre première stack.

`app/stacks/demo/main.tf`

```bash
resource "random_pet" "this" {
  length = 2
}

module "bucket" {
  source     = "../../modules/bucket_s3"
  bucket     = "bucket-${random_pet.this.id}"
  acl        = var.acl
}
```

La ressource `random_pet` génère des noms aléatoires d'animaux domestiques destinés à être utilisés comme identifiants uniques pour d'autres ressources.

On parle de module et de stacks, mais quel est la différence entre les deux. Les deux utilisent Terraform, mais la ou cela devient intéressant, c'est que la différence se fait au niveau de l'organisation.

Les modules sont généralement du code Terraform qui va nous permettre de créer des stacks. La ou Terraspace est intéressant, c'est qu'on peut créer plusieurs modules (rds, ec2, s3, route53, etc.) qui vont être utilisés pour nos stacks pour créer nos ressources.

Exemple : la stack (stack1) utilisera les modules rds et s3, mais la stack (stack2) pourra seulement créer une ressource rds. Cela, nous permet de séparer nos ressources, utiliser ce qui est nécessaire pour un projet spécifique et surtout réutiliser du code déjà créer (DRY).

## Déployer votre premier projet

Une fois notre arborescence et notre code construit. L'objectif va être de déployer notre première stack.

Sur votre terminal de commande :

```bash
terraspace up <nom_stack> (ici demo) -y
```

L'option **-y** est utilisé pour approuver le déploiement de notre infrastructure.

Vérifier ensuite que le bucket S3 a bien été crée sur la console AWS.

## Changement et mise à jour de notre infrastructure

Faisons quelques petites modifications sur les fichiers Terraform de notre infrastructure au niveau des variables, on déclarera la variable bucket qui n'avait pas été déclaré auparavant :

`app/stacks/demo/variables.tf`

```bash
variable "bucket" {
  description = "The name of the bucket. If omitted, Terraform will assign a random, unique name." # IE: terraform-2020052606510241590000000
  type        = string
  default     = null
}

variable "acl" {
  description = "The canned ACL to apply. Defaults to 'private'."
  type        = string
  default     = "private"
}
```

Ce qui est très intéressant avec `Terraspace`, c'est que vous pouvez utiliser un fichier **tfvars** pour définir les variables d'environnement ou selon la valeur de `TS_ENV`. Ici, on utilisera `TS_ENV=dev`. On le retrouve sous : **app/stacks/demo/tfvars**.

Pour générer un fichier **tfvars** : 

```bash
terraspace seed <nom_stack> (ici demo)
```

Les fichiers modifiées, il est temps de mettre à jour notre infrastructure. Toujours sur votre terminale de commande :

```bash
terraspace up <nom_stack> (ici demo) -y
```

Le résultat de notre modification :

```bash
Terraform used the selected providers to generate the following execution
plan. Resource actions are indicated with the following symbols:

  + create

Terraform will perform the following actions:
  # module.bucket.aws_s3_bucket_acl.this will be created
  + resource "aws_s3_bucket_acl" "this" {
      + acl    = "private"
      + bucket = "bucket-hopeful-tick"
      + id     = (known after apply)
    }
Plan: 1 to add, 0 to change, 0 to destroy.
```

## Détruire notre infrastructure

Après avoir vu ensemble comment utiliser Terraspace (créer un projet, modifier des fichiers, mettre à jour notre infrastructure, configurer notre environnement), l'infrastructure nous étant plus utile, il est maintenant temps de détruire nos ressources.

```bash
terraspace down <nom_stack> (ici demo) -y
```

Vérifier tout de même sur la console AWS que celles-ci ont bien été détruite. Dans la prochaine partie, nous irons plus loin dans l'utilisation de Terraspace notamment en faisant une démonstration avec de multiples stacks.

## Conclusion

En conclusion, Terraspace est un framework puissant pour la gestion d'infrastructure as a Code (IaC). Il offre de nombreux avantages comme la possibilité de déployer plusieurs stacks selon les modules pour éviter la duplication de code, la gestion de nos variables d'environnements et la compatibilité avec plusieurs fournisseurs de cloud.

Cependant, certains inconvénients doivent être pris en compte, tels que le manque de documentation. Bien que Terraspace dispose d'une documentation en ligne, certaines ressources vidéo sont payantes et les solutions aux problèmes rencontrés peuvent être difficiles à trouver. Il peut également être nécessaire de comprendre les différents dossiers du projet, les dépendances pour pouvoir utiliser pleinement Terraspace mais aussi l'exemple qui n'est pas suffisament optimiser dans le code.

En résumé, Terraspace peut être un excellent choix pour la gestion d'infrastructure en tant que code, mais il nécessite un certain investissement en temps et en apprentissage pour tirer le meilleur parti de ses fonctionnalités. Il existe d'autres types de framework pour la gestion d'infrastructure as a Code (IaC) pour terraform comme Terragrunt et d'autres outils comme Pulumi, Ansible.
